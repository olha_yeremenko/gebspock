package ui.pages

import geb.Page

class LoginPage extends Page {
    static url = "/"

    static at = { title == "Twitter. It's what's happening." }

    static content = {
        form { $(".front-signin") }
        email { form.$("#signin-email") }
        password { form.$("#signin-password") }
        submit { form.$(".submit") }
        loginChallengeInput (required:false){$('#login-challenge-form #challenge_response')}
        loginChallengeSubmit (required:false,to: TwitterHomePage) {$('#login-challenge-form #email_challenge_submit')}
    }

    def fillAndSubmitLoginForm(name, pass,phone){
        email << name
        password << pass
        submit.click()
        if(!loginChallengeInput.isEmpty()) {
            loginChallengeInput << phone
            loginChallengeSubmit.click()
        }
    }

}

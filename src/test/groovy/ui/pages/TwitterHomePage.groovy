package ui.pages

import geb.Page
import ui.pages.module.AlertPopupModule
import ui.pages.module.DashboardProfileCardModule
import ui.pages.module.HeaderModule
import ui.pages.module.NewTweetDialogModule
import ui.pages.module.TweetModule

class TwitterHomePage extends Page {

    static at = {
        dashboardProfile.displayed
    }

    static content = {
        dashboardProfile { module(DashboardProfileCardModule) }
        headerModule { module(HeaderModule) }
        newTweetDialog { module(NewTweetDialogModule) }
        alertPopup { module(AlertPopupModule) }
        tweetModuleItem { id -> module(new TweetModule(idNumber: id)) }


        tweetModuleList { $(".stream-container.conversations-enabled .tweet") }
        firstTweet { $(".stream-container.conversations-enabled .tweet", 0) }

    }
}

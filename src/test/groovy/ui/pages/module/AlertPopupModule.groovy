package ui.pages.module

import geb.Module

class AlertPopupModule extends Module {

    static content = {
        popup { $(".alert-messages") }
        closeBtn { $(".alert-messages .Icon--close") }
    }
}

package ui.pages.module

import geb.Module


class HeaderModule extends Module {

    static content = {
        newTweetBtn (to: NewTweetDialogModule) {$("#global-new-tweet-button") }
    }

}

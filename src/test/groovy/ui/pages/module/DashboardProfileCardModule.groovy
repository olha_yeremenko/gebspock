package ui.pages.module

import geb.Module


class DashboardProfileCardModule extends Module {

    static content = {
        block { $(".dashboard-left .DashboardProfileCard") }
        nameLink { $(".DashboardProfileCard-screenname a") }
    }

}

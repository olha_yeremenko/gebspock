package ui.pages.module

import geb.Page
import ui.pages.TwitterHomePage


class NewTweetDialogModule extends Page{
    static at = { $("#global-tweet-dialog").displayed }

    static content = {
        dialog { $("#global-tweet-dialog-dialog") }
        textInput {dialog.$(".RichEditor-container #tweet-box-global") }
        createBtn (to: TwitterHomePage) { dialog.$(".tweeting-text") }
        closeBtn { dialog.$(".modal-btn .Icon--close") }
    }

   def fillFormAndSubmit(String text){
        textInput << text
        createBtn.click()
    }
}

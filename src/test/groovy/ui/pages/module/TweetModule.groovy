package ui.pages.module

import geb.Module

class TweetModule extends Module {
    int idNumber

    static content = {
        block{$(".stream-container.conversations-enabled .tweet",idNumber) }
        textContent {block.$(".tweet-text") }
        username { block.$(".username") }
        tweetId { block.attr("data-tweet-id") }
        moreBtn { block.$(".Icon--caretDownLight") }
        actionsDelete { block.$(".js-actionDelete") }

        confimDeleteModal { $("#delete-tweet-dialog-dialog") }
        confimDeleteBtn { confimDeleteModal.$(".delete-action") }
    }
}

package ui

import base.BaseSpec
import spock.lang.Shared
import spock.lang.Stepwise
import ui.pages.LoginPage
import ui.pages.TwitterHomePage
import ui.pages.module.NewTweetDialogModule

@Stepwise
class UITestSpec extends BaseSpec {

    @Shared
    Properties props = loadProperties()

    def 'user should be logged in'() {
        given:
        to LoginPage
        when:
        fillAndSubmitLoginForm(props['mail'], props['pass'], props['phone'])
        then:
        at TwitterHomePage
        assert dashboardProfile.block.text().contains('@Test_Spock')
    }

    def 'user creates new tweet'() {
        given:
        at TwitterHomePage
        when:
        headerModule.newTweetBtn.click()
        at NewTweetDialogModule
        fillFormAndSubmit('user creates new tweet')
        then:
        at TwitterHomePage
        waitFor {alertPopup.popup.displayed==true}
    }

    def 'user check filed of created tweet'() {
        when:
        at TwitterHomePage
        then:
        def tweetContent= tweetModuleItem(0)
        assert tweetContent.username.text() == '@Test_Spock'
        assert tweetContent.textContent.text().contentEquals('user creates new tweet')
    }

    def 'user duplicate new tweet'() {
        given:
        at TwitterHomePage
        when:
        headerModule.newTweetBtn.click()
        at NewTweetDialogModule
        fillFormAndSubmit('user creates new tweet')
        then:
        at TwitterHomePage
        waitFor {alertPopup.popup.text().contains('You have already sent this Tweet.')}
        and:
        at NewTweetDialogModule
        assert dialog.displayed==true
        closeBtn.click()
        and:
        at NewTweetDialogModule
        waitFor{ dialog.displayed==false}
    }

    def 'user delete tweet'() {
        given:
        at TwitterHomePage
        when:
        def tweetContent= tweetModuleItem(0)
        tweetContent.moreBtn.click()
        waitFor{tweetContent.actionsDelete.displayed}
        tweetContent.actionsDelete.click()
        waitFor{tweetContent.confimDeleteBtn.displayed}
        tweetContent.confimDeleteBtn.click()
        then:
        at TwitterHomePage
        waitFor {alertPopup.popup.text().contains('Your Tweet has been deleted.')}
    }
}
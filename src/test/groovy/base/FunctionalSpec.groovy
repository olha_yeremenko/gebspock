package base

import api.TwitterApiSpec
import spock.lang.Shared
import ui.pages.LoginPage
import ui.pages.TwitterHomePage

class FunctionalSpec extends BaseSpec {

    @Shared
    TwitterApiSpec twitterApiSpec= new TwitterApiSpec()

    @Shared
    Properties props=loadProperties()

    def 'user should see tweet created via REST'() {
        given:
        to LoginPage
        fillAndSubmitLoginForm(props['mail'], props['pass'], props['phone'])
        at TwitterHomePage
        assert dashboardProfile.block.text().contains('@Test_Spock')
        when:
        String createdId=twitterApiSpec.createStatus('Test status')
        refreshPage()
        then:
        at TwitterHomePage
        and:
        def tweetContent= tweetModuleItem(0)
        assert tweetContent.tweetId == createdId
        assert tweetContent.username.text() == props['username']
        assert tweetContent.textContent.text().contentEquals('Test status')
    }

}

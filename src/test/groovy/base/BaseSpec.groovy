package base

import geb.spock.GebSpec
import groovyx.net.http.ContentType
import groovyx.net.http.RESTClient

class BaseSpec extends GebSpec {

    RESTClient restClient

    RESTClient getAuthorizedRestClient() {
        if (restClient == null) {
            Properties props =  loadProperties()
            restClient = new RESTClient(props['mainUrl'])
            restClient.auth.oauth props['consumerKey'], props['consumerSecret'], props['accessToken'], props['secretToken']
            restClient.contentType = ContentType.JSON
            restClient.headers.Accept = 'application/json'
        }
        restClient
    }

    def loadProperties() {
        Properties properties = new Properties()
            new File("src/test/resources/connection.properties").withInputStream {
                stream -> properties.load(stream)
            }
        properties
    }

    def cleanupSpec() {
        for (id in getAuthorizedRestClient().get(path: 'statuses/user_timeline').data.id) {
            getAuthorizedRestClient().post(path: "statuses/destroy/${id}.json").status == 200
            println "Delete tweet id: ${id}"
        }
    }

    void refreshPage() {
        browser.driver.navigate().refresh()
    }
}

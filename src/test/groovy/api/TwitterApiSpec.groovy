package api

import base.BaseSpec
import groovyx.net.http.HttpResponseException
import sun.invoke.empty.Empty

import static groovyx.net.http.ContentType.JSON
import static groovyx.net.http.ContentType.URLENC

class TwitterApiSpec extends BaseSpec {

    def 'Test GET method'() {
        when:
        def resp = getAuthorizedRestClient().get(path: 'statuses/home_timeline')
        then:
        assert resp.status == 200
        assert resp.contentType == JSON.toString()
        assert (resp.data instanceof List)
        assert resp.data.status.size() > 0
    }

    def 'Test GET method with fields verifying'() {
        when:
        def resp = getAuthorizedRestClient().get(path: 'statuses/home_timeline.json')
        then:
        resp.status == 200
        for (item in resp.data) {
            assert item.created_at != null
            assert item.retweet_count >= 0
            assert item.text != Empty
        }
    }

    def 'Test POST method. Update status'() {
        given: "user create new status"
        def beforeId = createStatus('Test status before updating')
        assert beforeId != null

        when:
        def afterId = createStatus('Test status after updating')

        then:
        assert beforeId != afterId
    }

    def 'Test POST method. Remove status'() {
        given: "user create new status"
        getAuthorizedRestClient().headers.Accept = 'application/json'
        def statusId=createStatus('Test status should be deleted')
        assert statusId != null

        when: "user delete created status"
        def respDel = getAuthorizedRestClient().post(path: "statuses/destroy/${statusId}.json")
        then:
        respDel.status == 200

        then: "user should not see deleted status in timeline"
        def respGet = getAuthorizedRestClient().get(path: 'statuses/home_timeline.json')
        respGet.status == 200
        assert respGet.data.id.contains(statusId) == false
    }

    def 'Test 403 error during tweet duplication'() {
        given: "create new status"
        createStatus('Tweet duplication')

        when:
        createStatus('Tweet duplication')

        then:
        final HttpResponseException ex = thrown()
        assert ex.statusCode == 403
    }

    def createStatus(String msg) {
        def newStatus = getAuthorizedRestClient().post(
                path: 'statuses/update.json',
                body: [status: msg],
                requestContentType: URLENC)
        assert newStatus.status == 200
        assert newStatus.headers.Status
        assert newStatus.data.text == msg
        newStatus.data.id
    }


}
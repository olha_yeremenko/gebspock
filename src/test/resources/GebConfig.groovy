import org.openqa.selenium.chrome.ChromeDriver

def chromeDriver = new File('src/test/resources/chromedriver.exe')
System.setProperty('webdriver.chrome.driver', chromeDriver.absolutePath)

driver = { new ChromeDriver() }

waiting {
    timeout = 15
    retryInterval = 0.5
    slow { timeout = 12}
    reallyslow { timeout = 24 }
}

baseNavigatorWaiting = true
atCheckWaiting = true

baseUrl = "https://twitter.com/"
